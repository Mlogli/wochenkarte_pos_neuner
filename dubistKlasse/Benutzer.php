<?php
$host = 'localhost:3306';
$username = 'root';
$passwd = '';
$dbname = 'wochenkarte';
//$con = new PDO('mysql:host=localhost;dbname=wochenkarte','root','');
$con = new mysqli($host, $username, $passwd, $dbname);

class Benutzer
{
    private $email = '';
    private $passwort ='';

    private $dubisteinFehler = [];
    

    public function __construct()
    {

    }



    public static function get($email, $passwort){
      /*  try {
            global $con;
            $sql = 'SELECT * FROM benutzer WHERE  Email = ? AND Passwort = ?';
            $stmt = $con->prepare($sql);
            $stmt->execute([$email,$passwort]);
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            if($data == null){
                print_r("findet nix in datenbank");
                return null;
            }
            else{
                $u= new Benutzer();
                $u->setEmail($data['Email']);
                $u->setPasswort($data['Passwort']);
            }
        }catch(PDOException $e){
            die($e->getMessage());
        }*/


        global  $con;
        if ($con->connect_errno) {
            die("Verbindung fehlgeschlagen: " . $mysqli->connect_error);
        }
        $query = "SELECT * FROM Benutzer WHERE Email = '%$email%' AND Passwort = '%$passwort%'";
        $result = mysqli_query($con, $query);

        $query2 = "SELECT * FROM benutzer";
        $result2 = mysqli_query($con, $query2);
        $benutzer = new Benutzer();
        if(mysqli_fetch_array($result2)==NULL){
            return null;
        } else {
            while ($row = mysqli_fetch_array($result2)) {
                $benutzer->setEmail($row[1]);
                $benutzer->setPasswort($row[2]);
            }
            print_r(
                "benutzer gefunden"
            );
            print_r($benutzer);
            return $benutzer;
        }


    }

    public function login(){
        $u = self::get($this->email, $this->passwort);
        if ($u != null){
            $_SESSION['email'] = $this->email;
            return true;
        }
        else{
            $this->dubisteinFehler['falsch'] = "Zugangsdaten gilten nicht";
            unset($_SESSION['email']);
            print_r("ES GIBT FALSE");
            return false;
        }

    }


    private function validateEmail()
    {
        if ($this->email != "" && !filter_var($this->email, FILTER_VALIDATE_EMAIL) && $this ->email < 5 && $this->email > 30) {
            $this->$dubisteinFehler['email'] = "E-Mail ungültig";
            return false;
        } else {
            return true;
        }
    }

    private function validatePasswort()
    {
        if ( $this ->passwort < 5 && $this->passwort > 20) {
            $this->$dubisteinFehler['passwort'] = "Passwort ungültig";
            return false;
        } else {
            return true;
        }
    }

    public function validate()
    {
        return $this->validateEmail() & $this->validatePasswort();
    }

    public static function logout()
    {
        unset($_SESSION['email']);
        session_destroy();
    }

    public static function isLoggedIn(){
        return isset($_SESSION['email']) && strln($_SESSION['email']) > 0;
    }








    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPasswort()
    {
        return $this->passwort;
    }

    /**
     * @param string $passwort
     */
    public function setPasswort($passwort)
    {
        $this->passwort = $passwort;
    }

    public function getErrors()
    {
        return $this->dubisteinFehler;
    }


}