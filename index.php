<?php

session_start();
require_once "./dubistKlasse/Benutzer.php";
require_once "./dubistKlasse/WillstduCookieWirHelfen.php";
$a = new Benutzer();
$message = '';


if (isset($_POST['cookie_submit'])) {
    WillstduCookieWirHelfen::createCookie();
}


if (isset($_POST['submit'])) {

    $a->setEmail(isset($_POST['email']) ? $_POST['email'] : '');
    $a->setPasswort(isset($_POST['passwort]']) ? $_POST['passwort'] : '');


    if ($a->validate()){
        if ($a->login()){
            header('Location: ./pages/wochenkarte.php');
            exit();
        }
        else {
            $message = "<div class='alert alert-danger'><p>Zugangsdaten ungültig!</p><ul>";
            foreach ($a->getErrors() as $key => $value) {
                $message .= "<li>" . $value . "</li>";
            }
            $message .= "</ul></div>";
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/cookiePopup.css">
    <style>
        .form {
            padding: 20px 0;
        }

        .form .form-group {
            text-align: left;
        }

        }
    </style>
    <title>Wochenkarte - Login</title>
</head>
<body>
<div class="container">
    <h1 class="mt-5 mb-3">Wochenkarte</h1>
    <br>
    <?php
    if (!WillstduCookieWirHelfen::cookieSet()) {
        ?>
        <form class="form-cookies" action="index.php" method="post">
            <div id="cookie-popup">
                <div class="hinweis">
                    <p>Wir verwenden Cookies. Durch die weitere Nutzung der Webseite stimmen Sie der Verwendung von
                        Cookies zu.</p>
                </div>

                <button name="cookie_submit">OK, ich bin einverstanden.</button>

            </div>
        </form>
        <?php
    } else {
        ?>


        <h3>Bitte anmelden</h3>
        <form action="index.php" method="post" class="form">
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label for="email">Email</label>
                    <input type="email"
                           name="email"
                           class="form-control"
                           required
                           value="">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4 form-group">
                    <label for="passwort">Passwort</label>
                    <input type="password"
                           name="passwort"
                           class="form-control"
                           value=""
                           required>
                </div>
            </div>


            <br>
            <div class="row">
                <div class="col-sm-3">
                    <button name="submit"
                            value="submit"
                            type="submit"
                            class="btn btn-block btn-primary">Anmelden
                    </button>
                </div>
            </div>
            <div>
            </div>
        </form>
        <?php
    }
    ?>
</div>
</body>
</html>

