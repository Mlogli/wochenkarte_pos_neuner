<?php
session_start();
?>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h1 align="center" class="mt-3 mb-5">Wochenkarte</h1>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 mb-3">
                <p>Montag</p>
            </div>
            <div class="col-sm-6 col-md-4 mb-3">
                <p>Dienstag</p>
            </div>
            <div class="col-sm-6 col-md-4 mb-3">
                <p>Mittwoch </p>
            </div>
            <div class="col-sm-6 col-md-4 mb-3">
                <p>Donnerstag</p>
            </div>
            <div class="col-sm-6 col-md-4 mb-3">
                <p>Freitag</p>
            </div>
            <div class="col-sm-6 col-md-4 mb-3">
                <p>Samstag</p>
            </div>
            <div class="col-sm-6 col-md-4 mb-3">
                <p>Sonntag</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
